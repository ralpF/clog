using System;




[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
public class CLogHintAttribute : Attribute
{
    /// <summary> color hash or name </summary>
    readonly public string color;

    public CLogHintAttribute(string defaultClassColor)
    {
        this.color = defaultClassColor;
    }

}