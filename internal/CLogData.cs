﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




namespace ColorLog.Internal
{
    internal static class CLogData
    {
       static internal Dictionary<string, string> colorSet;


        static CLogData()
        {
            var unityColorSet = new Dictionary<string, string>{
                { "aqua", "#00ffffff" },
                { "black", "#000000ff" },
                { "blue", "#0000ffff" },
                { "brown", "#a52a2aff" },
                { "cyan", "#00ffffff" },
                { "darkblue", "#0000a0ff" },
                { "fuchsia", "#ff00ffff" },
                { "green", "#008000ff" },
                { "grey", "#808080ff" },
                { "lightblue", "#add8e6ff" },
                { "lime", "#00ff00ff" },
                { "magenta", "#ff00ffff" },
                { "maroon", "#800000ff" },
                { "navy", "#000080ff" },
                { "olive", "#808000ff" },
                { "orange", "#ffa500ff" },
                { "purple", "#800080ff" },
                { "red", "#ff0000ff" },
                { "silver", "#c0c0c0ff" },
                { "teal", "#008080ff" },
                { "white", "#ffffffff" },
                { "yellow", "#ffff00ff" },
            };

            colorSet = new Dictionary<string, string>(unityColorSet);

        }


    }
}