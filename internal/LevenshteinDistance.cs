using System;
using System.Collections.Generic;

// using Levenshtein Distance algorithm for quantifying how dissimilar two strings are



namespace ColorLog.Internal
{
    internal static class StringDistance
    {
        static int LevenshteinDistance(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];
            if (n == 0) return m;
            if (m == 0) return n;

            for (int i = 0; i <= n; d[i, 0] = i++) ;
            for (int j = 0; j <= m; d[0, j] = j++) ;

            for (int i = 1; i <= n; i++)
                for (int j = 1; j <= m; j++)
                {
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }

            return d[n, m];
        }


        public static string MatchClosest(IEnumerable<string> collection, string target, int maxDistance)
        {
            if (collection == null || string.IsNullOrEmpty(target)) return null;

            int minDist = int.MaxValue;
            string winner = null;
            foreach (var s in collection)
            {
                int dist = LevenshteinDistance(s, target);
                if (dist == 0) return s;
                if (dist < minDist)
                {
                    minDist = dist;
                    winner = s;
                }
            }

            if (minDist > maxDistance) return null;
            return winner;
        }
    }
}