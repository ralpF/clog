﻿using System;
using System.Text;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;

using ColorLog.Internal;
using Object = UnityEngine.Object;
using ColorLog;

static public partial class CLog
{

    static private bool useMarkup_internal => Application.isEditor && Setup.useMarkup;
    static private Dictionary<Type, CallerInfo> typeDict = new Dictionary<Type, CallerInfo>();
    static StringBuilder sb = new StringBuilder(64);



    //.....................................................................................MAIN-FUNC

    static void LogInternal(string msg, string color, LogType type, Object context)
    {
        string callerMethodName = null;
        Type callerType = InferCallerType(3, out callerMethodName);

        CallerInfo callerInfo = GetCachedCallerInfo(callerType);
        callerInfo.methodName = callerMethodName;
        callerInfo.SetUserColor(color);

        AppendHeader(callerInfo);
        AppendMessage(msg, callerInfo);

        bool canPing = context != null;
        if (useMarkup_internal)
        {
            if (canPing) AppendPingText();
            if (callerInfo.Color.HasErrors) AppendErrorColor();
        }

        DispatchMessage(sb.ToString(), type, context);
        sb.Clear();
    }



    //..................................................................................PRIVATE-IMPL

    static void AppendHeader(CallerInfo info)
    {
        if (Setup.headerType == EHeaderType.None) return;

        if (useMarkup_internal)
        {
            sb.Append("<color=").Append(info.Color.DarkColorHash).Append('>');
            if (Setup.boldHeader)
                sb.Append("<b>");
        }

        if (Setup.boldHeader) sb.Append("<b>");
        sb.Append('[').Append(info.className);

        if (Setup.headerType == EHeaderType.ClassAndMethodName)
            sb.Append('.').Append(info.methodName);

        sb.Append("]: ");
        if (Setup.boldHeader) sb.Append("</b>");

        if (useMarkup_internal)
        {
            if (Setup.boldHeader)
                sb.Append("</b>");
            sb.Append("</color>");
        }
    }


    static private void AppendMessage(string msg, CallerInfo info)
    {
        if (useMarkup_internal)
            sb.Append("<color=").Append(info.Color.MainColorHash).Append('>');

        sb.Append(msg);

        if (useMarkup_internal)
            sb.Append("</color>");
    }


    static private void AppendPingText()
    {
        sb.Append("  <color=grey><size=8>(Ping)</size></color>");
    }


    private static void AppendErrorColor()
    {
        sb.Append(" <color=grey><size=8>(Err Color)</size></color>");
    }


    static private Type InferCallerType(int level, out string methodName)
    {
        var frame = new System.Diagnostics.StackFrame(level, false);
        methodName = "Undefined";
        Type type = null;

        if (frame != null)
        {
            var method = frame.GetMethod();
            if (method != null)
            {
                methodName = method.Name;
                type = method.DeclaringType;
            }
        }
        return type;
    }


    static private CallerInfo GetCachedCallerInfo(Type t)
    {
        if (t == null)
            return CallerInfo.Undefined;

        CallerInfo result;
        if (typeDict.TryGetValue(t, out result)) return result;

        // no key in dictionary, add
        result = new CallerInfo(t);
        typeDict[t] = result;
        return result;
    }


    static private void DispatchMessage(string message, LogType logType, Object context)
    {
        bool canPing = context != null;
        switch (logType)
        {
            case LogType.Log:
                if (canPing) Debug.Log(message, context);
                else         Debug.Log(message);
                break;

            case LogType.Error:
                if (canPing) Debug.LogError(message, context);
                else         Debug.LogError(message);
                break;

            case LogType.Warning:
                if (canPing) Debug.LogWarning(message, context);
                else         Debug.LogWarning(message);
                break;

            case LogType.Assert:
                if (canPing) Debug.LogAssertion(message, context);
                else         Debug.LogAssertion(message);
                break;

            case LogType.Exception:
            default:
                throw new System.NotImplementedException(logType.ToString());
        }
    }
}
