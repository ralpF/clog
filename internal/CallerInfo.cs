using System;



namespace ColorLog.Internal
{
    internal struct CallerInfo
    {
        static private CColor defaultLogColor = new CColor(Setup.defaultColor);

        public string className;
        public string methodName;                                 // method name is set from outside
        private CColor userColor;
        private CColor attributeColor;
        public CColor Color => userColor.IsSet ? userColor : (attributeColor.IsSet ? attributeColor : defaultLogColor);



        public CallerInfo(Type t)
        {
            // init this object struct
            methodName = null;
            className = null;
            userColor = CColor.None;
            attributeColor = CColor.None;


            // TODO: add assertion for t
            string typeName = t.ToString();
            this.className = ExtractClassName(typeName);
            // search for attribute
            InferColorsFromClassAttribute(t);
        }


        public void SetUserColor(string colorNameOrHash)
        {
            userColor = new CColor(colorNameOrHash);
        }

        private string ExtractClassName(string s)
        {
            int index = s.LastIndexOf('.');
            string className = index != -1 ? s.Substring(index + 1) : s;

            // fix <> md issue in unity console painter
            int squareTokenIdxStart = className.IndexOf("+<");
            if (squareTokenIdxStart != -1)
                className = className.Substring(0, squareTokenIdxStart);
            return className;
        }

        private void InferColorsFromClassAttribute(Type t)
        {
            object[] attribArr = t.GetCustomAttributes(typeof(CLogHintAttribute), true);
            if (attribArr.Length > 0)
            {
                var a = (CLogHintAttribute)attribArr[0];
                this.attributeColor = new CColor(a.color);
            }
        }

        public static CallerInfo Undefined = new CallerInfo { className = "Undefined", methodName = "Undefined" };
    }
}