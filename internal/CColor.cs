﻿using System.Collections.Generic;





namespace ColorLog.Internal
{
    public struct CColor
    {

        //...................................................................................PRIVATE

        string _hash;
        string _darkHash;
        bool   _hasErrors;
        bool   _isDefaultColor;

        //....................................................................................PUBLIC

        public bool   IsSet => !string.IsNullOrEmpty(_hash) && !_isDefaultColor;
        public bool   HasErrors => _hasErrors;

        public string MainColorHash => _hash;
        public string DarkColorHash => GetDarkColorHash();


        //......................................................................................CTOR

        internal CColor(string colorNameOrHsh)
        {
            _hash = null;
            _darkHash = null;
            _hasErrors = false;
            _isDefaultColor = false;
            SetColorValue(colorNameOrHsh);
        }

        //...................................................................................PRIVATE

        private void SetColorValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                value = Setup.defaultColor;
                _isDefaultColor = true;
            }

            _hash = ParseString(value);

            if (_hasErrors && Setup.WillFixNameErrors)
            {
                int maxErrors = Setup.colorNameMaxErrorCount;
                string closest = StringDistance.MatchClosest(CLogData.colorSet.Keys, value, maxErrors);
                if (closest != null)
                    _hash = CLogData.colorSet[closest];
            }
        }

        private string ParseString(string value)
        {
            if (string.IsNullOrEmpty(value)) throw new System.InvalidOperationException();
            if (CLogData.colorSet.TryGetValue(value, out var result)) return result; // hash by name
            if (IsHexString(ref value)) return value;           // color hash, will add # if missing
            _hasErrors = true;                    // if we are here then the name or hash is invalid
            return null;
        }

        private string GetDarkColorHash()
        {
            if (string.IsNullOrEmpty(_hash)) throw new System.InvalidOperationException();
            if (_hash.Length != 7 && _hash.Length != 9) throw new System.InvalidOperationException($"val:{_hash}");
            if (_darkHash == null)
            {
                string si = _hash.Substring(1, 6);
                int ic = int.Parse(si, System.Globalization.NumberStyles.HexNumber);
                // multiply channels
                int ir = (int)( (ic >> 16 & 0xFF) * Setup.headerColorMultiplier);
                int ig = (int)( (ic >> 8  & 0xFF) * Setup.headerColorMultiplier);
                int ib = (int)( (ic & 0xFF)       * Setup.headerColorMultiplier);
                // clamp
                ir = ir < 255 ? ir : 255; ig = ig < 255 ? ig : 255; ib = ib < 255 ? ib : 255;
                // :X2 convert to hex with 2 leading zeroes. this happen once
                _darkHash = string.Format("#{0:X2}{1:X2}{2:X2}ff", ir, ig, ib);
            }
            return _darkHash;
        }



        //....................................................................................STATIC

        public static CColor None => new CColor();

        static bool IsHexString(ref string s)
        {
            if (string.IsNullOrEmpty(s)) return false;

            bool prefixed = s[0] == '#';
            int len = s.Length;
            if (prefixed) len--;

            if (len != 6 && len != 8) return false;

            for (int i = prefixed ? 1 : 0; i < s.Length; ++i)
            {
                char ch = s[i];
                bool ok = ch >= '0' && ch <= '9'
                    || ch >= 'a' && ch <= 'f'
                    || ch >= 'A' && ch <= 'F';
                if (!ok) return false;
            }
            // if we are here, this is a hex string
            // if hashtag is missing, add it
            s = prefixed ? s : "#" + s;
            if (s.Length == 9) s = s.Substring(0, 7);
            return true;
        }

    }
}