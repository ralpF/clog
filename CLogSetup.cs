﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





namespace ColorLog
{
    public static class Setup
    {
        static public bool useMarkup = true;
        static public bool boldHeader = false;
        static public string defaultColor = "grey";
        static public EHeaderType headerType = EHeaderType.ClassName;

        static public float headerColorMultiplier = 1.5f;

        /// <summary>
        /// Max Levenshtein distance to fix errors. Set to 0 to disable error correction </summary>
        static public int colorNameMaxErrorCount = 3;

        static public bool WillFixNameErrors => colorNameMaxErrorCount > 0;

    }
}
