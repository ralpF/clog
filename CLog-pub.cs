#define CLOG_ASSERT_ON

using UnityEngine;
using ColorLog.Internal;



static public partial class CLog
{

    //........................................................................................PUBLIC

    public static void Info(string message)
    {
        LogInternal(message, null, LogType.Log, null);
    }


    public static void Info(string message, string color)
    {
        LogInternal(message, color, LogType.Log, null);
    }


    public static void Info(string message, Object pingObj)
    {
        LogInternal(message, null, LogType.Log, pingObj);
    }


    public static void Info(string message, string color, Object pingObj)
    {
        LogInternal(message, color, LogType.Log, pingObj);
    }


    [System.Diagnostics.Conditional("CLOG_ASSERT_ON")]
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    public static void Trace(string message)
    {
        LogInternal(message, "black", LogType.Log, null);
    }


    public static void Error(string message, string color, Object pingObj)
    {
        LogInternal(message, color, LogType.Error, pingObj);
    }



    public static void Error(string message, string color)
    {
        LogInternal(message, color, LogType.Error, null);
    }


    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    public static void TestPrint()
    {
        foreach (var kv in CLogData.colorSet)
            CLog.Info($"{kv.Key} The quick brown fox", kv.Key);
    }

}